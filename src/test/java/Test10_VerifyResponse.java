import io.restassured.matcher.ResponseAwareMatcher;
import io.restassured.response.Response;
import org.hamcrest.Matcher;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

public class Test10_VerifyResponse {
    /**
     * status code verification
     */
    @Test
    public void testStatusInResponse() {
        given().get("http://jsonplaceholder.typicode.com/photos").then().assertThat().statusCode(200).log().all();
        given().get("http://jsonplaceholder.typicode.com/photos").then().assertThat().statusLine("HTTP/1.1 200 OK");
        given().get("http://jsonplaceholder.typicode.com/photos").then().assertThat().statusLine(containsString("OK"));
    }


    /**
     * headers verification
     */
    @Test
    public void testHeadersVerification() {
        given().get("http://jsonplaceholder.typicode.com/photos").then().assertThat().header("X-Powered-By", "Express");
        given().get("http://jsonplaceholder.typicode.com/photos").then().assertThat()
                .headers("Vary","Origin, Accept-Encoding","Content-Type", containsString("json"));
    }

    /**
     * body text verification
     */
    @Test
    public void testBodyInResponse() {
        String responseString  = get("http://www.thomas-bayer.com/sqlrest/CUSTOMER/02/").asString();
        given().get("http://www.thomas-bayer.com/sqlrest/CUSTOMER/02/").then().assertThat().body(equalTo(responseString));
    }

    /**
     * body attribute verification with Java 8
     */

    @Test
    public void testBodyParametersInResponse() {
        //Java 7:
//        given()
//                .get("http://jsonplaceholder.typicode.com/photos/1")
//        .then()
//            .body("thumbnailUrl", new ResponseAwareMatcher<Response>() {
//                public Matcher<?> matcher(Response response) {
//                    return equalTo("https://via.placeholder.com/150/92c952");
//                }
//            });

    //Java 8:
        given().get("http://jsonplaceholder.typicode.com/photos/1")
                .then()
                .body("thumbnailUrl", response -> equalTo("https://via.placeholder.com/150/92c952"));

        given().get("http://jsonplaceholder.typicode.com/photos/1").then().body("thumbnailUrl", endsWith("92c952"));

    }



}
