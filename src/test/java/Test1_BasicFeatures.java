import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

public class Test1_BasicFeatures {

    @Test
    public void testStatusCode() {
        given().get("http://jsonplaceholder.typicode.com/posts/3")
                .then()
                .statusCode(200);
    }

    /**
     * verify code and print the complete response
     */
    @Test
    public void testLogging() {
        given()
                .get("http://services.groupkt.com/country/get/iso2code/IN")
                .then()
                .statusCode(200)
                .log().all();
    }

    /**
     * Verify single content using org.harmcrest.Matchers library's equalTo method
     */
    @Test
    public void testEqualToFunction() {
        given()
                .get("http://services.groupkt.com/country/get/iso2code/US")
                .then()
                .body("RestResponse.result.name", equalTo("United States of America"));
    }

    /**
     * Verify multiple content using org.hamcrest.Matchers library
     */
    @Test
    public void testHasItemFunction() {
        given()
                .get("http://services.groupkt.com/country/get/all")
                .then()
                .body("RestResponse.result.name", hasItems("Argentina", "Australia"));
    }

    /**
     * Parameters and headers can be set
     */
    @Test
    public void testParametersAndHeaders() {
        given()
                .param("key1", "value1")
                .header("headA", "valueA")
                .when()
                    .get("http://services.groupkt.com/country/get/iso2code/GB")
                .then()
                    .statusCode(200)
                    .log()
                    .all();
    }


    /**
     * Using "and" to increase readability
     * generally used when writing in one line xpath style
     */
    @Test
    public void testAndFeatureForReadability() {
        given().param("key1", "value1")
                .and()
                .header("key2","value2")
                .when()
                .get("http://services.groupkt.com/country/get/iso2code/CN")
                .then()
                .statusCode(200)
                .and()
                .body("RestResponse.result.alpha3_code", equalTo("CHN"))
        .log().all();
    }
}
