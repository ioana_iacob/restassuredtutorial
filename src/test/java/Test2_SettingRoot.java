import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.is;

public class Test2_SettingRoot {

    /**
     * Basic way to test all parameters
     */
    @Test
    public void testWithoutRoot() {
        given()
                .get("http://services.groupkt.com/country/get/iso2code/IT")
        .then()
                .body("RestResponse.result.name", is("Italy"))
                .body("RestResponse.result.alpha2_code", is("IT"))
                .body("RestResponse.result.alpha3_code", is("ITA"));
    }


    /**
     * Recommended way to test all parameters using root feature
     */
    @Test
    public void testWithRoot(){
        when()
                .get("http://services.groupkt.com/country/get/iso2code/IT")
        .then()
            .root("RestResponse.result")
            .body("name", is("Italy"));
    }

    @Test
    public void testDetachedRoot() {
        given()
                .get("http://services.groupkt.com/country/get/iso2code/IT")
        .then()
                .root("RestResponse.result")
                .body("name", is("Italy"))
                .detachRoot("result")
                .body("result.alpha3_code", is("ITA"));
    }
}
