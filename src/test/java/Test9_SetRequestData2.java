import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.equalTo;

/**
 * This class is to set different type of data in request call
 *
 */

public class Test9_SetRequestData2 {
    /**
     * To path parameters type 2
     */
    @Test
    public void testSetPathParameters2() {
        given()
                .pathParam("type","json")
                .pathParam("section", "Domains")
        .when()
                .post("http://api.fonts.com/rest/{type}/{section}")
        .then()
            .statusCode(400);
    }

    /**
     * Cookies can be set in request param
     */
//    @Test
//    public void testSetCookiesInRequest() {
//        //to set single values
//        given()
//                .cookie("__utmt", "1")
//        .when()
//                .get("http://www.webservicex.com/globalweather.asmx?op=GetCitiesByCountry")
//        .then()
//            .statusCode(200);
//    }

    /**
     * Multiple cookies can be set in request param
     */
    @Test
    public void testSetMultiCookiesInRequest() {
        //to set multi value
        given().cookie("key", "val1", "val2"); //this creates two cookies key = val1; key=val2;

        //to set detailed cookie
        Cookie cookie = new Cookie.Builder("some_cookie", "some_value").setSecured(true).setComment("comm").build();
        Cookie cookie2 = new Cookie.Builder("some_cookie", "some_value").setSecured(true).setComment("comm").build();
        Cookies cookies = new Cookies(cookie,cookie2);
        given().cookies(cookies).when().get("/cookie").then().assertThat().body(equalTo("c"));
    }

    /**
     * We can pass single headers, headers with multiple values and multiple headers
     */
    @Test
    public void testSetHeaders () {
        given()
                .header("k","v")
                .header("k10", "val1", "val2", "val3")
                .headers("k1","v1", "k2", "v2")
        .when()
            .get("https://api.fonts.com/rest/json/Accounts/")
        .then()
            .statusCode(400);
    }

    /**
     * Content type can be also set
     */
    @Test
    public void testSetContentType() {
        given()
                .contentType(ContentType.JSON)
                .contentType("application/json; charset=utf-8")
        .when()
            .get("https://api.fonts.com/rest/json/Accounts/")
        .then()
            .statusCode(400);
    }

}
