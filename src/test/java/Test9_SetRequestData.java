import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.*;

public class Test9_SetRequestData {

    /**
     * Generally CONNECT used with HTTPS request
     *
     * refer: http://stackoverflow.com/questions/11697943/when-should-one-use-connect-and-get-http-methods-at-http-proxy-server
     */
    @Test
    public void testConnectRequest() {
        when()
                .request("CONNECT", "https://api.fonts.com/rest/json/Accounts/")
        .then()
            .statusCode(400);
    }

    /**
     * in GET request we can set query parameter
     */
    @Test
    public void testQueryParameters() {
        given()
                .queryParam("A", "A Val")
                .queryParam("B", "B val")
        .when()
                .get("http://api.fonts.com/rest/json/Accounts/")
        .then()
                .statusCode(400);
    }

    /**
     * in POST request we can set form parameter
     */
    @Test
    public void testFormParameters() {
        given()
                .formParam("A", "A val")
                .formParam("B", "B val")
        .when()
                .post("http://api.fonts.com/rest/json/Domains/")
        .then()
                .statusCode(400);
    }

    /**
     * To set parameters - recommended way !! (not the ones above)
     * If request is GET then param will be treated as QueryParameter
     * If request is POST then param will be treated as FormParameter
     */
    @Test
    public void testSetParameters() {
        given()
                .param("A", "A val")
                .param("B", "B val")
        .when()
                .post("http://api.fonts.com/rest/json/Accounts/")
        .then()
                .statusCode(400);
    }

    /**
     * To set multiple value parameters
     *
     * We can pass list, multiple values or no values in param
     */
    @Test
    public void testSetMultipleParameters() {
        List<String> list = new ArrayList<String>();
        list.add("one");
        list.add("two");

        given()
                .param("A","val1","val2","val3")
                .param("B")
                .param("C",list)
        .when()
                .get("https://api.fonts.com/rest/json/Accounts/")
        .then()
                .statusCode(400);
    }
}
