import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class Test5_ReadResponseInDiffWays {

    /**
     * To get all response as String
     */
    @Test
    public void testGetResponseAsString() {
        String responseAsString = get("http://services.groupkt.com/country/search?text=lands").asString();
        System.out.println("My Response:\n\n\n" + responseAsString);
    }

    /**
     * To get all response as InputStream
     * @throws IOException
     */
    @Test
    public void testGetResponseAsInputStream() {
        InputStream stream = get("http://services.groupkt.com/country/get/iso2code/CN").asInputStream();
        System.out.println("Stream lenght" + stream.toString().length());
        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get all response as ByteArray
     */
    @Test
    public void testGetResponseAsByteArray() {
        byte[] byteArray = get("http://services.groupkt.com/country/get/iso2code/CN")
                .asByteArray();
        System.out.println(byteArray.length);
    }

    /**
     * extract details using path
     */
    @Test
    public void testExtractDetailsUsingPath() {
        String href =
                when()
                .get("http://jsonplaceholder.typicode.com/photos/1")
                .then()
                .contentType(ContentType.JSON)
                .body("albumId", equalTo(1))
                .extract()
                .path("url");

        System.out.println(href);
        when().get(href).then().statusCode(200);
    }

    /**
     * Extract details using path in one line
     */
    @Test
    public void testExtractPathInOneLine() {
        //type1:
        String href1 = get("http://jsonplaceholder.typicode.com/photos/1").path("thumbnailUrl");
        System.out.println("Fetched URL 1: " + href1);
        when().get(href1).then().statusCode(200);

        //type2:
        String href2 = get("http://jsonplaceholder.typicode.com/photos/1").andReturn().jsonPath().getString("thumbnailUrl");
        System.out.println("Fetched URL 2: " + href2);
        when().get(href2).then().statusCode(200);

    }

    /**
     * Extract details as Response for further use
     */
    @Test
    public void testExtractDetailsUsingResponse() {
        Response response =
                when()
                    .get("http://jsonplaceholder.typicode.com/photos/1")
                .then()
                .extract().response();
        System.out.println("Content Type " + response.contentType());
        System.out.println("Href " + response.path("url"));
        System.out.println("Status Code: " + response.statusCode());
    }
}
