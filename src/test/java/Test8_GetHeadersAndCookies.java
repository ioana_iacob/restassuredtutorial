import io.restassured.http.Cookie;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.*;

public class Test8_GetHeadersAndCookies {
    /**
     * To get response headers
     */
    @Test
    public void testResponseHeaders() {
        Response response = get("http://jsonplaceholder.typicode.com/photos");

        //to get a single header
        String headerCFRAY = response.getHeader("CF-RAY");
        System.out.println(">>>>>> Header: " + headerCFRAY + "\n");

        //to get all headers
        Headers headers = response.getHeaders();
        for (Header header:headers) {
            System.out.println(header.getName() + ":" +header.getValue());
        }
    }

    /**
     * To get cookies
     */
    @Test
    public void testCookies() {
        Response response = get("http://jsonplaceholder.typicode.com/photos");
        Map<String,String> cookies = response.getCookies();
        for(Map.Entry<String, String> entry : cookies.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
    }

    /**
     * To get detailed cookies
     */
    @Test
    public void testDetailedCookies() {
        Response response = get("http://jsonplaceholder.typicode.com/photos");
        Cookie c = response.getDetailedCookie("__cfduid");
        System.out.println("Detailed: " + c.hasExpiryDate());
        System.out.println("Detailed: " + c.getExpiryDate());
        System.out.println("Detailed: " + c.hasValue());
    }
}
