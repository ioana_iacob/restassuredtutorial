import io.restassured.path.json.JsonPath;
import org.testng.annotations.Test;

import java.util.List;

import static io.restassured.RestAssured.*;
import static io.restassured.path.json.JsonPath.from;

public class Test7_JsonPath {
    /**
     * Extract details as String and fetching further details w/o using json path
     */
    @Test
    public void testJsonPath1() {
        String responseAsString =
                when()
                    .get("http://jsonplaceholder.typicode.com/photos")
                .then()
                    .extract()
                    .asString();
        List<Integer> albumsId = from(responseAsString).get("id");
        System.out.println(albumsId.size());
    }

    /**
     * Extract details as String and fetching further details using JsonPath
     */
    public void testJsonPath2() {
        String json = when()
                        .get("http://services.groupkt.com/country/get/all")
                    .then()
                        .extract()
                        .asString();

        JsonPath jsonPath = new JsonPath(json).setRoot("RestResponse.result");
        List<String> list = jsonPath.get("name");
        System.out.println(list.size());
    }

}

